#!/usr/bin/env python3

# On Fedora requires: python3-lark-parser python3-beautifulsoup4


from urllib.request import urlopen, Request
from bs4 import BeautifulSoup as BS
import argparse
import json
import re
from pprint import pprint
import lark
import sys

TSIZE = 10
CHSIZE = 12
LSIZE = CHSIZE + 4
PT = 0.3527777778


FONT = "Courier"


parser = argparse.ArgumentParser()
parser.add_argument('url', type=str, nargs=1,
                    help='Url of the song')
parser.add_argument('--trans', type=int, default=0, help="Choose transposition")
parser.add_argument('--dumb', default=False, action="store_true",
        help="Run in dumb mode")
parser.add_argument('--pdf', type=str, nargs="?", help="Choose output pdf")
parser.add_argument('--text-size', type=int, default=TSIZE, help="PDF text size (pt)")
parser.add_argument('--chord-size', type=int, default=CHSIZE, help="PDF chord size (pt)")
parser.add_argument('--line-size', type=int, default=LSIZE, help="PDF linefeed size (pt)")
parser.add_argument('--margin', type=int, default=20, help="PDF margin size (mm)")


def pt2mm(pt):
    return pt * PT

def mm2pt(mm):
    return mm / PT

def app():

    args = parser.parse_args()
    url = args.url[0]

    req = Request(url)
    req.add_header('User-Agent', 'Mozilla/5.0 (X11; Linux x86_64; rv:107.0) Gecko/20100101 Firefox/107.0')
    resp = urlopen(req)
    soup = BS(resp, "html.parser")
    js=json.loads(soup.find(
        ["div"],
        {"class":"js-store"}).get_attribute_list("data-content")[0]
    )


    data = js['store']['page']['data']["tab_view"]["wiki_tab"]["content"]
    versions = js['store']['page']['data']["tab_view"]["versions"]

    if args.dumb and args.trans != 0:
        print("Dumb node does not support transposition", file=sys.stderr)
        exit(1)


    artist = None
    song_name = None
    if len(versions) > 0:
        version = versions[0]
        artist = version["artist_name"]
        song_name = version["song_name"]

    if args.dumb:
        dumb(data, args, song_name, artist)
    else:
        recursive(data, args, artist, song_name)

def dumb(data, args, artist_name="", song_name=""):
    data = re.sub('\[ch\]', '', data)
    data = re.sub('\[/ch\]', '', data)
    data = re.sub('\[tab\]', '', data)
    data = re.sub('\[/tab\]', '', data)

    if args.pdf:
        from fpdf import FPDF
        pdf = FPDF("P", "pt", "A4")
        pdf.add_page()
        pdf.set_font("Courier", size = args.text_size)

        pdf.cell(0, args.text_size, txt=artist_name, ln=1)
        pdf.cell(0, args.text_size, txt=song_name, ln=1)
        pdf.cell(0, args.text_size, txt="", ln=1)
        for line in data.split("\n"):
            pdf.cell(0, args.line_size, txt=line, ln=1)
        pdf.output(args.pdf)

    else:
        print(song_name)
        print(artist)
        print(data)


def transpose(tree, trans):

    # TODO: H is sometimes marked as B
    tones = ["C", "C#","D","D#","E","F","F#","G","G#","A","A#","H","H#", "C"]

    for n in tree.iter_subtrees_topdown():
        if n.data != "chord_text":
            continue

        chord = n.children[0].value
        mtchs = re.findall("([CDEFGAHC]#?)", chord)

        subs = []
        for i in mtchs:
            num = trans
            #TODO handle erros
            num += tones.index(i)
            num %= 12
            if num < 0:
                num = 12 + num

            subs.append((i, tones[num]))

        # TODO fix problem with multiple chords.
        #      Chord already transposed may be changed again
        for i in subs:
            chord = re.sub(i[0], i[1], chord, count=1)

        n.children[0].value = chord

    return tree



class PdfPrinter():
    def __init__(self, artist, song_name, tsize, chsize, lsize, margin):
        from fpdf import FPDF
        self.chsize = chsize
        self.tsize = tsize
        self.lsize = lsize
        self.margin = mm2pt(margin)

        self.pdf = FPDF("P", "pt", "A4")
        self.pdf.add_page()
        self.pdf.set_margins(self.margin, self.margin, self.margin)
        if artist and song_name:
            self.pdf.set_font(FONT, size=TSIZE+4, style="B")
            self.pdf.set_x(self.margin)
            self.pdf.cell(self.pdf.get_string_width(artist), TSIZE+4, artist)
            self.pdf.ln(TSIZE+6)
            self.pdf.cell(
                    self.pdf.get_string_width(song_name),
                    TSIZE+4,
                    song_name
            )
            self.pdf.ln(TSIZE+6)
            self.pdf.ln(TSIZE+6)

        self.pdf.set_font(FONT, size=TSIZE, style="")


    def process_text(self, tree):

        for node in tree.children:
            self.process_text_node(node)

    def process_text_node(self, node):

            strs = node.value.split('\n')

            for j in range(len(strs)):

                # don't print empty strings
                if len(strs[j]) != 0:
                    self.pdf.cell(self.pdf.get_string_width(strs[j]), TSIZE, strs[j])

                if j != len(strs) - 1:
                    self.pdf.ln(self.lsize)


    def process_tab(self, tree):



        self.pdf.set_auto_page_break(False)
        for ch in tree.children:
            if not isinstance(ch, lark.Tree):
                continue
            if ch.data == "text":
                self.process_text(ch)
            elif ch.data == "chord":
                self.process_chord(ch)
        self.pdf.set_auto_page_break(
                True,
                margin=self.margin + self.lsize + self.chsize + self.tsize
        )
        self.pdf.cell(0,0," ")

    def process_chord(self, tree):
        for ch in tree.children:
            if not isinstance(ch, lark.Tree):
                continue
            if ch.data == "chord_text":
                self.process_chord_text(ch)

    def process_chord_text(self, tree):

            for node in tree.children:
                x = self.pdf.get_x()
                norm_width = self.pdf.get_string_width(node.value)
                self.pdf.set_font(FONT, size=self.chsize, style="BI")
                self.process_text_node(node)
                self.pdf.set_font(FONT, size=self.tsize, style="")
                self.pdf.set_x(x + norm_width)




    def process_tree(self, tree):

        for ch in tree.children:
            if not isinstance(ch, lark.Tree):
                continue
            if ch.data == "text":
                self.process_text(ch)

            elif ch.data == "tab":
                self.process_tab(ch)

            elif ch.data == "chord":
                self.process_chord(ch)


    def write_pdf(self, filename):
        self.pdf.output(filename)



def print_txt(tree, args):

    def isleaf(node):
        return node.data == "chord_text" or node.data == "text"

    txt=""

    for i in filter(isleaf, tree.iter_subtrees_topdown()):
            txt += i.children[0].value

    print(txt)


def recursive(data, args, artist, song_name):

    data = re.sub('\[ch\]', '<ch>', data)
    data = re.sub('\[/ch\]', '</ch>', data)
    data = re.sub('\[tab\]', '<tab>', data)
    data = re.sub('\[/tab\]', '</tab>', data)
    data = re.sub('\r', '', data)



    l = lark.Lark('''start: (text | tab | chord)+
                     text: TEXT
                     TEXT: /[^<>]+/
                     tab: TAB_OPEN (text | chord)* TAB_CLOSE
                     TAB_OPEN: "<tab>"
                     TAB_CLOSE: "</tab>"
                     chord: CHORD_OPEN chord_text CHORD_CLOSE
                     chord_text: TEXT
                     CHORD_OPEN: "<ch>"
                     CHORD_CLOSE: "</ch>"
    ''')


    tree = l.parse(data)
    tree = transpose(tree, args.trans)


    if args.pdf:
        pdf_printer = PdfPrinter(
                artist,
                song_name,
                tsize=args.text_size,
                chsize=args.chord_size,
                lsize=args.line_size, margin=args.margin
        )
        pdf_printer.process_tree(tree)
        pdf_printer.write_pdf(args.pdf)

    else:
        if song_name and artist:
            print(song_name)
            print(artist)
            print()
        print_txt(tree, args)


if __name__ == "__main__":
    app()







