Name:           ugchords
Version:        0.0.7
Release:        1%{?dist}
Summary:        Ultimate guitar tabs downloader

License:        GPLv2+
#URL:            https://pagure.io/fedpkg
Source0:        %{name}-%{version}.tar.gz

BuildArch:      noarch

BuildRequires:  python3-setuptools
BuildRequires:  python3-rpm-macros

Requires: python3-lark-parser
Requires: python3-beautifulsoup4
Requires: python3-fpdf


%description
Web app to show payments for a flat


%prep
%setup -q

%build
%{__python3} setup.py build


%install
%{__python3} setup.py install -O1 --skip-build --root $RPM_BUILD_ROOT


%clean
rm -rf $RPM_BUILD_ROOT


%files
%doc LICENSE
%{_bindir}/%{name}
# For noarch packages: sitelib
%{python3_sitelib}/*


%changelog
* Thu Feb 23 2023 Matej Mužila <mmuzila@redhat.com> 0.0.7-1
- Send User-Agent (mmuzila@redhat.com)

* Thu Oct 07 2021 Matej Mužila <mmuzila@redhat.com> 0.0.6-1
- Require python3-fpdf (mmuzila@redhat.com)

* Thu Oct 07 2021 Matej Mužila <mmuzila@redhat.com> 0.0.5-1
- Require python3-fpdf

* Wed Oct 06 2021 Matej Mužila <mmuzila@redhat.com> 0.0.5-1
- Add option to export PDF (mmuzila@redhat.com)

* Mon Oct 04 2021 Matej Mužila <mmuzila@redhat.com> 0.0.4-1
- Add dependencies (mmuzila@redhat.com)

* Mon Oct 04 2021 Matej Mužila <mmuzila@redhat.com> 0.0.3-1
- Update spec (mmuzila@redhat.com)

* Mon Oct 04 2021 Matej Mužila <mmuzila@redhat.com> 0.0.2-1
- new package built with tito

* Mon Oct 04 2021 Matej Mužila <mmuzila@gmail.com> - 0.0.1-1
- Initial commit
