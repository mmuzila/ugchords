# -*- coding: utf-8 -*-

from setuptools import setup, find_packages

#with open('README.rst') as f:
#    readme = f.read()

with open('LICENSE') as f:
    license = f.read()

setup(
    name='ugchords',
    version='0.0.7',
    description='Ultimate guitar tabs downloader',
    #long_description=readme,
    author='Matej Mužila',
    author_email='mmuzila@gmail.com',
    scripts=['bin/ugchords'],
    #url='https://github.com/kennethreitz/samplemod',
    license=license,
    include_package_data=True,
    packages=find_packages(exclude=('tests', 'docs'))
)
